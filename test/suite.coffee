"use strict"

Apphire = require 'apphire'

if window?
  window.app = new Apphire()
  app.request.base = 'http://localhost:4000'
  app._getSocketAdress = -> #just for mocking
    return 'ws://localhost:4000'
  restart = ->
    prepareRunner()
    
    setTimeout (-> require('./main').add(); runTests()), 2000 #Give some for backend relaunch
  restart()
  if module.hot then module.hot.accept restart
else  
  eval 'global.Test = require("apphire-test")'
  global.app = new Test()
  cfg =
    execMap:
      js: "node --inspect"
  app.spawnBackend('./test/backend.js', cfg)
  app.serve entry: './test/suite'

