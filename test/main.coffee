if app.isClient
  window._sendErrorTimeoutMock = 5000
  MessageQueue = require '../lib'
  MessageQueue.initialize app

  module.exports =
    add: ->
      describe "init", ->        
        before ->
          yield app.launch()
          yield MessageQueue.connect()

        describe 'session basics', ->
          sessionId = null
          it 'creates session', ->
            sessionId = yield app.socket.request 'createSession'
            sessionId.should.be.string
            (yield app.socket.request 'getSessionById', sessionId).should.equal sessionId

        describe 'session expiration', ->
          sessionId = null
          it 'session expired', ->
            sessionId = yield app.socket.request 'createSession'
            yield new Promise (resolve)-> setTimeout resolve, 400
            yield throws app.socket.request('heartbeat', sessionId), /expired/

        describe 'connections', ->
          it 'connected', ->
            (yield MessageQueue.connect()).should.equal MessageQueue
            MessageQueue.sessionId.should.be.string
          
          it 'disconnected', ->
            sessionId = MessageQueue.sessionId
            yield MessageQueue.disconnect()
            yield throws app.socket.request('getSessionById', sessionId), /expired/

          it 'emits session expired', ->
            yield new Promise (resolve)-> async ->
              MessageQueue.once 'sessionExpired', resolve
              yield MessageQueue.connect()
              yield new Promise (resolve)-> setTimeout resolve, 400
              MessageQueue.pushToQueue("something")


      describe 'server-client queue', ->
        before ->
          yield app.launch()
        beforeEach ->
          yield MessageQueue.connect()
          
        it 'one element', ->
          yield new Promise (resolve, reject)-> async ->
            MessageQueue.onTransaction = (transaction)->
              if transaction is "one" then resolve()
            yield app.socket.request('sendTransactionsToSession', {sessionId: MessageQueue.sessionId, transactions: ["one"]})
        
        it 'three in a row', ->
          yield new Promise (resolve, reject)-> async ->
            trs = []
            transactions = ["one", "two", "three"]
            MessageQueue.onTransaction = (transaction)->
              trs.push transaction 
              if trs.length is 3
                try
                  trs.should.equal.transactions
                  resolve()
                catch e
                  reject()
            yield app.socket.request('sendTransactionsToSession', {sessionId: MessageQueue.sessionId, transactions: transactions})

        it 'emits error when processing queue', ->
          yield new Promise (resolve, reject)-> async ->
            transactions = ["one", "two", "BUGGY", "four", "five"]
            errMsg = "This is the buggy one"
            MessageQueue.onTransaction = (transaction)->
              if transaction is "BUGGY" then throw new Error errMsg
            MessageQueue.once "error", (e)->
              try
                e.type.should.equal "ServerClientTransaction"
                e.message.should.equal errMsg
                e.transaction.should.equal "BUGGY"
                resolve()
              catch e
                reject e
              
            yield app.socket.request('sendTransactionsToSession', {sessionId: MessageQueue.sessionId, transactions: transactions})
        
      describe 'client-server queue', ->   
        before ->
          yield app.launch()     
        beforeEach ->
          yield app.socket.request('resetTransactionCount')
          yield MessageQueue.connect()
          
        it 'one element', ->          
          transaction = "something"            
          MessageQueue.pushToQueue(transaction)
          yield new Promise (resolve)-> setTimeout resolve, 300
          (yield app.socket.request('getCurrentTransaction')).should.equal transaction
          (yield app.socket.request('getTransactionCount')).should.equal 1

        it 'two in a row', ->  #this actually will be processed as two socket requests by design   
          transactions = ["something1", "something2"]      
          transactions.forEach (t)-> MessageQueue.pushToQueue(t)
          yield new Promise (resolve)-> setTimeout resolve, 300
          (yield app.socket.request('getCurrentTransaction')).should.equal transactions[1]
          (yield app.socket.request('getTransactionCount')).should.equal 2

        it 'three in a row', ->   #and this also initiates only two request but second one will contain second and third transaction    
          transactions = ["something1", "something2", "something3"]          
          transactions.forEach (t)-> MessageQueue.pushToQueue(t)
          yield new Promise (resolve)-> setTimeout resolve, 300
          (yield app.socket.request('getCurrentTransaction')).should.equal transactions[2]
          (yield app.socket.request('getTransactionCount')).should.equal 3


        it 'emits error when processing queue', ->   #and this also initiates only two request but second one will contain second and third transaction    
          yield app.socket.request('setNodeEnv', "development")
          transactions = ['one', 'two', 'BUGGY', 'four', 'five']
          yield new Promise (resolve, reject)-> async ->
            MessageQueue.once 'error', (e)-> async ->
              try
                e.type.should.equal "ClientServerTransaction"
                e.stack.should.be.string
                e.transaction.should.equal "BUGGY"
                resolve()
              catch e
                reject e
              
            transactions.forEach (t)-> MessageQueue.pushToQueue(t)

        it 'no stacktraces leaked to user in prod mode if error when processing queue', ->   #and this also initiates only two request but second one will contain second and third transaction    
          transactions = ['BUGGY']
          yield app.socket.request('setNodeEnv', "production")
          yield new Promise (resolve, reject)-> async ->
            MessageQueue.once 'error', (e)-> async ->
              try
                expect(e.stack).to.be.undefined
                resolve()
              catch e
                reject e
              
            transactions.forEach (t)-> MessageQueue.pushToQueue(t)
          yield app.socket.request('setNodeEnv', "development")
                             
else
  process.on 'unhandledRejection', (r)=> console.log(r)
  process.env.NODE_ENV = "development"
  global._sessionTimeoutMock = 300
  MessageQueue = require '../lib'
  currentTransaction = null
  transactionCount = 0

  app.socket.response 'sendTransactionsToSession', ({sessionId, transactions})->
    s = MessageQueue.loadSession(sessionId, @)
    transactions.map (t)->
      s.pushToQueue t
    return 

  app.socket.response 'setNodeEnv', (NODE_ENV)->
    process.env.NODE_ENV = NODE_ENV

  app.socket.response 'getSessionCount', ()->
    return (s for s of MessageQueue.sessions).length

  app.socket.response 'getSessionById', (sessionId)->
    return MessageQueue.loadSession(sessionId, @).sessionId

  app.socket.response 'getCurrentTransaction', ()-> 
    return currentTransaction

  app.socket.response 'getTransactionCount', ()->
    return transactionCount

  app.socket.response 'resetTransactionCount', ()->
    transactionCount = 0
    
  MessageQueue.initialize app, (transaction)->
    if transaction is 'BUGGY' then throw new Error 'This is the buggy one'
    transactionCount += 1
    currentTransaction = transaction