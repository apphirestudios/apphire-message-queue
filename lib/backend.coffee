sendErrorTimeout = global._sendErrorTimeoutMock or 10000
sessionTimeout = global._sessionTimeoutMock or 30000

debug = (msg)->
  console.log msg

{guid, hasOwn, extend, isFunction, isArray, isPlainObject, delay, interval} = require 'apphire-helpers'

class Session
  constructor: (@QueueManager, @sessionId, @connection)->
    @queue = []
    @updateHeartbeat()

  delete: ->
    @enqueuedForDeletion = true
    delete @QueueManager.sessions[@sessionId]
  
  updateHeartbeat: ->
    if @clearSessionTimeout then clearTimeout @clearSessionTimeout
    @clearSessionTimeout = delay sessionTimeout, =>
      @delete()

    @heartbeat = new Date()

  pushToQueue: (message)->
    @queue.push message

    if @queue.length is 1 #if queue was empty, then no queue sending functions currently active, let's start one
      @sendQueue()

  sendQueue: -> async => #this is function which will re-invoke itself (with timeout in case of error) until queue is empty
    return if @enqueuedForDeletion #if we want to delete the session then all the sending processes will stop here
    try
      response = yield @connection.request 'queue', @queue
      @queue.splice 0, response.processed
      if @queue.length > 0 then @sendQueue()
    catch e
      delay sendErrorTimeout, @sendQueue.bind(@)

  enqueuedForDeletion: false
  sessionId: null
  clearSessionTimeout: null
  connection: null

class MessageQueue
  sessions: {}
  loadSession: (sessionId, connection)->
    s = @sessions[sessionId] or throw new Error "Session id #{sessionId} not exist. It may be expired. Call createSession to create new one."
    s.connection = connection
    s.updateHeartbeat()
    return s
  
  deleteSession: (sessionId)->
    @sessions[sessionId].delete()

  initialize: (@app, @onTransaction)->
    self = @

    @app.socket.response 'createSession', ->
      sessionId = guid()
      self.sessions[sessionId] = new Session(self, sessionId, @) #TODO 'this' var should contain connection, check apphire's socket method
      return sessionId

    @app.socket.response 'deleteSession', (sessionId)->
      self.loadSession sessionId, @ #just for checking that it still exists
      self.deleteSession sessionId

    @app.socket.response 'heartbeat', (sessionId)->
      s = self.loadSession sessionId, @
      return sessionId

    @app.socket.response "queue", ({sessionId, queue})->
      s = self.loadSession sessionId, @
      response = {returned: []}
      for transaction in queue
        try
          returned = yield async self.onTransaction(transaction, s)
        catch e
          returned = {error: e.toString()}
          if process.env.NODE_ENV is "development" then returned.stack = e.stack
        
        response.returned.push returned
      response.processed = response.returned.length
      response

module.exports = new MessageQueue()