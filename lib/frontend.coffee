Emitter = require 'apphire/lib/emitter'
sendErrorTimeout = window._sendErrorTimeoutMock or 30000

{guid, hasOwn, extend, isFunction, isArray, isPlainObject, delay, interval} = require 'apphire-helpers'


class MessageQueue extends Emitter
  initialize: (@app, @onTransaction)->
    @queue = []
    @app.socket.response 'queue', (queue)=> async =>      
      response = {returned: []}
      for transaction in queue
        try
          yield async @onTransaction(transaction)
        catch e
          err = new Error e.message
          err.transaction = transaction
          err.stack = e.stack
          err.type = "ServerClientTransaction"
          @emit 'error', err

      response.processed = 0
      return response
  
  enqueuedForDisconnecting: false  
  connect: -> async =>
    @enqueuedForDisconnecting = false
    @sessionId = yield @app.socket.request 'createSession'
    return @

  disconnect: ->
    @enqueuedForDisconnecting = true
    yield @app.socket.request 'deleteSession', @sessionId

  pushToQueue: (message)->
    @queue.push message
    if @queue.length is 1 #if queue was empty, then no queue sending functions currently active, let's start one
      @sendQueue()

  sendSingle: (message)-> async =>
    transaction = message
    response = yield @app.socket.request "single", {@sessionId, transaction}
    if (ret = response.returned[0])?.error
      err = new Error ret.error          
      err.stack = ret.stack
      err.type = "ClientServerTransaction"
      throw err
    return response.returned[0]


  sendQueue: -> async => #this is function which will re-invoke itself (with timeout in case of error) until queue is empty
    return if @enqueuedForDisconnecting
    try
      response = yield @app.socket.request "queue", {@sessionId, @queue}
      for ret, index in response.returned
        if ret?.error 
          err = new Error ret.error          
          err.transaction = @queue[index]
          err.stack = ret.stack
          err.type = "ClientServerTransaction"
          @emit "error", err

      @queue.splice 0, response.processed
      if @queue.length > 0 then @sendQueue()
    catch e
      if /expired/.test e.message
        @queue = []
        @emit 'sessionExpired' 
      else
        delay sendErrorTimeout, @sendQueue.bind @

module.exports = new MessageQueue()